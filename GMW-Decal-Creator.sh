#!/bin/bash
#
# Fantasy Grounds Session Decal Generator
# by GM Wintermute (https://www.gmwintermute.com)
#
# You are free to modify this script.  Just give me credit by leaving in my announcement text.  Thanks
#

#Version Number
VERSION=1   

#Your name
AUTHOR="GM Wintermute"

#Prefix inserted before extension name
EXTENSIONPREFIX="GMW"   

#Which font to use
SESFONT=/usr/share/fonts/truetype/mason-sans/MasonSansRegular_Regular.ttf

#The text desplayed before the session number on the decal
SESTEXT="Session"

#Announcement displayed for this extension when Fantasy Grounds is started.
YOURANNOUNCEMENT="Your announcement here."

#filename of the icon to use when displaying YOURANNOUNCEMENT
YOURICON=graphics/icons/GMW-FG-token.png

#Text color of the SESTEXT and session number
TEXTCOLOR=white

#Name of session template image
TEMPLATEIMAGE=template.png


#Do not edit below this line.

BEGINNUM=$1
ENDNUM=$2


rm OptionsManager.txt
rm StringText.txt
rm DecalFile.txt
rm extension.txt
rm -fr graphics/decals/*

mkdir graphics
mkdir graphics/decals

for i in `seq $BEGINNUM $ENDNUM`;
do
echo "Generating image #$i of $ENDNUM"
convert template.png  -font $SESFONT -pointsize 54  -draw "gravity south fill black  text 0,12 '$SESTEXT $i' fill $TEXTCOLOR  text 1,11 '$SESTEXT $i' " graphics/decals/$i.png

OPTIONSMANAGER="OptionsManager.addOptionValue(\"DDCL\", \"option_val_DDCL_gmwcustom$i\", \"desktopdecal_gmwcustom$i\", true);"
echo "            $OPTIONSMANAGER" >> OptionsManager.txt
                l
STRINGTEXT="<string name=\"option_val_DDCL_gmwcustom$i\">$SESTEXT $i</string>"
echo "  $STRINGTEXT" >> StringText.txt

DECALFILE=" <icon name=\"desktopdecal_gmwcustom$i\" file=\"graphics/decals/$i.png\" />"
echo " $DECALFILE" >> DecalFile.txt

done
echo "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>"> extension.xml
echo "<root version=\"3.0\" logo=\"logo.png\">">> extension.xml
echo "<announcement text=\"$YOURANNOUNCEMENT\" icon=\"youricon\" font=\"emotefont\" />">> extension.xml
echo "<announcement text=\" Created using the Session Decal Creator script from GM Wintermute http://www.gmwintermute.com/2in4\"  icon=\"gmwicon\" font=\"emotefont\" />">> extension.xml

echo "<properties>">> extension.xml
echo "  <name>$EXTENSIONPREFIX Session Decals $BEGINNUM to $ENDNUM </name>">> extension.xml
echo "  <version>$VERSION</version>">> extension.xml
echo "  <author>$AUTHOR</author>">> extension.xml
echo "  <description>Adds Session Number Decals to the desktop</description>">> extension.xml
echo "  <loadorder>99</loadorder>">> extension.xml
echo "</properties>">> extension.xml
echo "<base>">> extension.xml
echo "  <icon name=\"gmwicon\" file=\"graphics/icons/GMW-FG-token.png\" />">> extension.xml
echo "  <icon name=\"youricon\" file=\"$YOURICON\" />">> extension.xml
echo "  <script name="SessionDecals">">> extension.xml
echo "      function onInit()">> extension.xml
cat OptionsManager.txt >> extension.xml
echo "      end">> extension.xml
echo "  </script>">> extension.xml
echo "  <!-- Custom theme option strings -->">> extension.xml
cat StringText.txt >> extension.xml
echo "  <!-- Custom Decal Images -->" >>extension.xml
cat DecalFile.txt >> extension.xml       
echo "  </base>" >>extension.xml
echo "</root>" >>extension.xml
rm OptionsManager.txt
rm StringText.txt
rm DecalFile.txt

